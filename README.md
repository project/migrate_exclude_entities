# Migrate: Exclude Entities

Allows specific entity types to be excluded from the Drupal migration. This can
be useful to avoid errors for entity types which were used on a Drupal 7 site
but which are no longer needed on the new Drupal 8 or 9 site.

## Requirements

Requires the Migrate Drupal module provided by core. Works with either Migrate
Drupal UI provided by core, or
[Migrate Upgrade](https://www.drupal.org/project/migrate_upgrade) and
[Migrate Tools](https://www.drupal.org/project/migrate_tools).

## Supported

The following plugins are supported by the module:

* d7_field
* d7_field_instance
* d7_field_instance_widget_settings
* d7_field_formatter_settings
* d7_view_modes

## Configuration

In the site's `settings.php` file define an array struture named
`$settings['migrate_exclude_entities']` which lists entity types to be
excluded, each assigned the boolean `TRUE`.

As an example, in order to exclude the "misc_things" and "megamenu_things"
entity types, add the following to the settings.php file:

`// Migrate Exclude Entities: Exclude the following entity types.
$settings['migrate_exclude_entities'] = [
  'misc_things' => TRUE,
  'megamenu_things' => TRUE,
];`

## Credits / contact

Written and maintained by [Damien McKenna](https://www.drupal.org/u/damienmckenna).

The best way to contact the author is to submit an issue, be it a support
request, a feature request or a bug report, in the
[project's issue queue](https://www.drupal.org/project/issues/migrate_exclude_entities).
